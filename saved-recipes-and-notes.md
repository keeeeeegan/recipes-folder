recipes we know we like

## Turmeric Chicken

http://www.budgetbytes.com/2011/10/turmeric-chicken/

Modifications:
* powdered ginger (1/4-1/2 tsp)
* no onion

## Chicken Noodle Soup

http://www.foodnetwork.com/recipes/anne-burrell/chicken-noodle-soup-recipe.html

* Use two containers of 33% less sodium chicken broth
* Cook lightly thawed frozen chicken in george foreman for around 6 minutes
* shred the chicken
* You can get sliced carrots in a bag, just toss em in

## Baja Sauce / White Sauce

Great with fish tacos. Adapted/taken from: http://www.foodnetwork.com/recipes/baja-fish-tacos-recipe.html?oc=linkback

White Sauce:
* 1 cup mayonnaise **(I only used 1/2 cup)**
* 1/4 cup milk
* 4 tablespoons lemon juice
* 1 teaspoon garlic salt
* Add some Cayenne (pinch(es))

Whisk together non lemon juice ingredients. Then, add lemon and shake until thick (shake using a mason jar, or like a tightly-sealed tupperware)

## Sunny's Cole Slaw
* Ethnic isle chow mein noodles, thinnest
* White wine vinegar
* Coleslaw mix

* Red bell pepper
* Green onion, 6tbsp diagonal cut
* Sliced almonds, toast
* 2tbsp roasted sesame seeds

* 4tbsp sugar
* 6tbsp rice (wine) vinegar
* 1 tsp pepper
* Half tsp salt
* 10 tbsp olive


## "Beef" Mushroom Stroganoff

http://m.allrecipes.com/recipe/60923/portobello-mushroom-stroganoff/?page=0

* 1/2 cup sour cream instead of 1.5
* +1/2 can of tomato sauce
* +garlic


## Gluten-Free Banana Bread

http://minimalistbaker.com/one-bowl-gluten-free-banana-bread/

* used soy and coconut flours instead of almond meal and the gluten free blend
* used two bananas because that's all we had
