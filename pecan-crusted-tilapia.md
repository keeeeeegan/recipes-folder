* 3-4 tilapia or mahi-mahi fillets
* 1/2 tsp salt
* 1/2 tsp pepper
* 1 cup panko
* 1 cup chopped pecans
* 2 eggs (beaten)
* 2 tbsp butter, 2 tbsp oil
* lemon juice

mix salt, pepper, panko, and pecans

melt butter and oil in pan

dip fish in egg, then in panko mix, then onto pan

cook on med to med-high heat for 4-5 mins per side

plate fish

add lemon juice, salt, pepper, for taste

adapted from: http://www.niftythriftysavings.com/pecan-crusted-tilapia-w-tarragon-green-beans-clean-eating/
