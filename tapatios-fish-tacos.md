you mexi-can create this delicious seafood dinner

--

## tilapia fillets
rub in oil, salt and pepper

heat oil in pan on high

cook on high for about 3-4 minutes per side, so the outside is crispy

take off heat, put shredded mozzarella on fillets (not too much so that it doesn’t melt)

cover with cookie sheet or something so the cheese melts (ya dingus)

## shrimp
heat oil in pan high

add shrimp, add lemon juice, let water boil off

add salt and pepper

add garlic

add more lemon juice

continue to cook until dark crispies

## refried beans
vegetarian refried beans

+cheddar

microwave beans, add cheese, mix in, microwave again

## yellow rice
prepare per package instructions

## extras
* slice avocado
* salsas
* sour cream
* also tortillas (corn and/or flour)

## *bonus* onions and peppers:
heat oil on high

add onions, cook until desired

add salt and pepper

add peppers and garlic, cook until desired

more salt and pepper? garlic salt?
