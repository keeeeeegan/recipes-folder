## Sweet Potato Tortilla Soup

http://www.budgetbytes.com/2014/12/sweet-potato-tortilla-soup/

Modifications:

* substitute onions with sliced portobellos
* cook for just a bit longer than 30 minutes, and on a slightly higher temp
* used purple sweet potatoes instead of orange

### Dec 9.

I ended up cooking it for a bit longer than the recipe calls for. I also simmered it on a slightly higher temp. I was afraid the potatoes would not get soft because we were using more potatoes than the recipe calls for.

Served with a heated tortilla , avocado slices, and greek yogurt (instead of sour cream). Also discovered the potato skins are really good roasted in the oven as a side item.

Recommendations for next time: add more corn!
