* fakemeat of your choice:
  * tempeh
  * beyond chicken strips
  * fake chicken nuggets
  * tofu??? (haven't tried tofu in this dish yet)
* jar of pasta sauce
* one spaghetti squash
* mozzarella cheese
* feta cheese
* parmesean cheese
* olive oil
* garlic salt
* salt and pepper

## Spaghetti squash

preheat the oven to 425

cut the spaghetti squash in half and scoop out the inside using a grapefruit spoon
drizzle/brush with in olive oil, and sprinkle salt and pepper (to taste, I guess)

lay face down on a cookie sheet covered with aluminum foil or parchment paper

cook at 425 for about 50 minutes (the backside of the squash will get dark brown in some areas)

take it out of the oven, flip the halves over, and using two forks...uh, fork, away the squash from the skin. it should be really easy and look like pasta. I've found when spaghetti squash comes out of the oven, and it's good, it looks a little translucent.

## Fakemeat prep

drizzle a little olive oil in a pan on medium-high heat

while it's cooking, throw on garlic salt, mrs dash, red pepper cumin...any kind of spice you think would go well with pasta sauce

cook on medium-high heat until desired golden-brown-ness

## Assemble

microwave the spaghetti sauce for about 1-3 minutes in the microwave

make a small mound of squash*, similar to the amount of spaghetti noodles you would use. sprinkle feta cheese, just a little bit.

spoon out enough of the pasta sauce to almost cover the mound

then layer on strips of the fake meat. next, sprinkle a layer of mozzarella cheese, more feta, and then the parmasean. all in amounts you think you'll like

this next step is optional, but I like to then put the mounds back in the oven (*since I know I'm going to put them back in the oven, I assemble these on a silicone pan so I can just slide them in the oven) on broil to try and melt some of the cheese. this happens really, really fast. so you have to watch it. pull them out as soon as you think they look good.

serve

spaghetti squash prep: http://www.marthastewart.com/1048761/roasted-spaghetti-squash
