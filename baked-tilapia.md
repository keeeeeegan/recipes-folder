* 2 tilapia fillets
* mayo
* paprika, parsley, herbs
* salt and pepper, to taste

1. add some oil to baking dish
2. place frozen tilapia in dish
3. spread mayo on fish
4. sprinkle on herbs/spices
5. oven 400
6. cook in oven for 15-20 minutes
