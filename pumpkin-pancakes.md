* 1/2 cup whole wheat flour
* 1/2 + 1/4 cups bisquick biscuit mix
* 1 tbsp dark brown sugar
* 1 tbsp coconut sugar
* pinch kosher salt
* 1/4 tbsp pumpkin pie spice
* 1 cup cashew milk (silk brand, unsweetened)
* 1/2 canned pumpkin
* 1 egg
* 2 tbsp vegetable oil

1. In a large bowl, whisk flour, brown sugar, baking powder, salt, and pumpkin pie spice.
2. In a medium bowl, whisk milk, pumpkin, egg, and vegetable oil.
3. Pour the wet ingredients over the flour mixture and whisk gently until completely combined. Set aside for 5 minutes.
4. Griddle/pan to medium heat.
5. Ladle ⅓-cup of the batter onto the griddle for each pancake. Cook for 2 to 3 minutes, or until the edges are set and the bubbles around the edges are open and set. Flip and cook on the second side for an additional 2 minutes, or until golden brown.

adapted from: http://www.browneyedbaker.com/pumpkin-pancakes-recipe/
