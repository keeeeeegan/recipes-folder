* fillet(s) tilapia (fresh or thawed)
* oil
* garlic
* lemon juice
* salt/garlic salt (optional)

put oil in pan on medium-ish (5-6)

cook fish without touching for 4 minutes. while this is happening, drizzle lemon juice and sprinkle salt

flip, cook for 3-4 minutes on other side, adding lemon juice and salt as neccessary

sprinkle garlic on this side. flip and cook for a little longer to cook the garlic and possibly brown a side
